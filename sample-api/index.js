import express from 'express';
import { getData } from "./util/s3.js";

const app = express();
const port = 3000;

app.get('/:tenantId/data', async (req, res) => {
  const { tenantId } = req.params;
  const { email } = req.query;

  // Get synced data
  const data = await getData(tenantId) || [];
  // Get matching contact
  const contact = data.find(d => d?.emailaddress1 === email) || {};

  return res.send([contact]);
});

app.listen(port, () => console.log(`API listening on port ${port}!`))
