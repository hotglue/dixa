import { S3Client, GetObjectCommand } from "@aws-sdk/client-s3";

const client = new S3Client({ region: "us-east-1" });

const S3_BUCKET_NAME = "testing.misc.hotglue.xyz";

export const getData = async (tenantId) => {
    try {
        const command = new GetObjectCommand({
            Bucket: S3_BUCKET_NAME,
            Key: `dixa/${tenantId}/contacts.json`
        });

        const { Body } = await client.send(command);
        const bodyContents = await streamToString(Body);

        return JSON.parse(bodyContents);
    } catch (e) {
        console.error(e);
    }
}

const streamToString = (stream) =>
    new Promise((resolve, reject) => {
        const chunks = [];
        stream.on("data", (chunk) => chunks.push(chunk));
        stream.on("error", reject);
        stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
    });
